//
//  DKAppDelegate.h
//  WWPD
//
//  Created by Vincent Palcon on 4/4/13.
//  Copyright (c) 2013 WWPD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
