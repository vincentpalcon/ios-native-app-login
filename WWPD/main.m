//
//  main.m
//  WWPD
//
//  Created by Vincent Palcon on 4/4/13.
//  Copyright (c) 2013 WWPD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DKAppDelegate class]));
    }
}
